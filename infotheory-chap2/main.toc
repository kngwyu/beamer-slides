\beamer@subsectionintoc {0}{1}{Subsection 2.1: Entropy}{4}{0}{0}
\beamer@subsectionintoc {0}{2}{Subsection 2.2: Joint and Conditional Entropy}{11}{0}{0}
\beamer@subsectionintoc {0}{3}{Subsection 2.3: Relative Entropy and Mutual Information}{16}{0}{0}
