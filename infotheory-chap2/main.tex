\documentclass[aspectratio=169]{beamer}
\usepackage{amsmath}
\usepackage[most]{tcolorbox}

\usetheme{focus}
\definecolor{main}{RGB}{86, 10, 225}

\title{Information Theory Reading Group}
\subtitle{Chapter2: Entropy, Relative Entropy, and Mutual Information}

\author{Yuji Kanagawa}
\date{04 11 2021}

% Footline info is printed only if [numbering=fullbar].
%\footlineinfo{Custom footline text}

\usepackage{amsmath}	% required for `\align*' (yatex added)
\begin{document}
    \begin{frame}
        \maketitle
    \end{frame}

    \begin{frame}{Contents}
      \begin{itemize}
        \item About this reading group
        \item Definitions: Entropy, KL divergence, and MI
        \item Basic inequalities
      \end{itemize}
    \end{frame}

    \begin{frame}{About this reading group}
      \begin{itemize}
        \item Read 'Elements of Information Theory'
        \item A presenter presents about a chapter or a subset of chapter
        \item Once in two weeks
        \item Share your motivation!
      \end{itemize}
    \end{frame}

    \subsection{Subsection 2.1: Entropy}
    \begin{frame}{Random Variable}
      Random variable $X$ is a function ($g(X)$) that \textbf{maps an event $x$ to a measurable space}

      Notations:
      \begin{description}
        \item[Alphabet]: $\mathcal{X}$ (= a space consting of all events)
        \item[Probability math function $p$]: $p(x) = \Pr\{X=x\} (x\in \mathcal{X})$
        \item[Random variable with $\mathcal{X}$ and $p$]: $X$
      \end{description}
    \end{frame}

    \begin{frame}{Entropy}
      \begin{tcolorbox}[title=Definition]
        Entropy of a discrete random variable $X$ is
        \begin{align*}
          H(X) = -\sum_{x\in\mathcal{X}}p(x)\log_2 p(x)
        \end{align*}
      \end{tcolorbox}
      Example: Fair coin toss
      \begin{align*}
        H(X) &= -\frac{1}{2} \log_2 \frac{1}{2} -\frac{1}{2}  \log_2 \frac{1}{2} \\
             &= 1 ~~\textrm{bit}
      \end{align*}
      \begin{itemize}
        \item $H_b$ denotes $-\sum_{x\in\mathcal{X}}p(x)\log_b p(x)$
        \item We use 2 as $b$ unless otherwise noted
      \end{itemize}
    \end{frame}

    \begin{frame}{Expectation}
      Expected value of the random variable $g(X)$ is
      \begin{align*}
        E_p g(X) = \sum_{x\in\mathcal{X}} g(x) p(x)
      \end{align*}
      \begin{tcolorbox}[title=Remark]
        \begin{align*}
          H(X) = E_p \log\frac{1}{p(X)}
        \end{align*}
      \end{tcolorbox}
    \end{frame}

    \begin{frame}{Nonnegativity of entropy}
      $H(X) \geq 0$ since $\log \frac{1}{p(x)} \geq 0$
      \begin{figure}
        \centering
        \includegraphics[width=8cm]{log1-logp.pdf}
      \end{figure}
    \end{frame}

    \begin{frame}{Change the base of log}
      \begin{align*}
        H_b(X) &= E_p \left[ \log_b\frac{1}{p(X)} \right]\\
               &= E_p \left[ \log_b a \log_a \frac{1}{p(X)} \right] \\
               &= \log_b a E_p \left[ \log_a \frac{1}{p(X)} \right]\\
               &= \log_b a H_a(X) \\
      \end{align*}
    \end{frame}

    \begin{frame}{Maximum entropy?}
      Example: $\mathcal{X} = \{a, b\}$
      \begin{figure}
        \centering
        \includegraphics[width=8cm]{p-H.pdf}
      \end{figure}
    \end{frame}

    \subsection{Subsection 2.2: Joint and Conditional Entropy}
    Consider another alphabet $\mathcal{Y}$.

    $p(x, y)$ denotes the probability of having $x$ and $y$ at the same time.
    \begin{frame}{Joint Entropy}
      \begin{tcolorbox}[title=Definition]
        Joint entropy of a pair of random variables $(X, Y)$ with $p(x, y)$ is
        \begin{align*}
          H(X, Y) &= -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(x, y) \\
                  &= -E \log p(x, y) \\
        \end{align*}
      \end{tcolorbox}
    \end{frame}

    \begin{frame}{Conditional Entropy}
      $p(y|x)$ denotes the probability of having $y$ when we have $x$
      \begin{tcolorbox}[title=Definition]
        Conditional entropy  is
        \begin{align*}
          H(Y|X) &= -\sum_{x\in\mathcal{X}}p(x) H(Y|X=x) \\
                 &= -\sum_{x\in\mathcal{X}}p(x) \sum_{y\in\mathcal{Y}} p(y|x) \log p(y|x) \\
                 &= -\sum_{x\in\mathcal{X}} \sum_{y\in\mathcal{Y}} p(x, y) \log p(y|x) \\
                 &= -E \log p(x, y) \\
        \end{align*}
      \end{tcolorbox}
    \end{frame}

    \begin{frame}{Chain Rule}
      \begin{tcolorbox}[title=Theorem]
        \begin{align*}
          H(X,Y) &= -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(x, y) \\
                 &= -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(x) p(y|x) \\
                 &= -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(x)
                    -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(y|x) \\
                 &= -\sum_{x\in\mathcal{X}}p(x)\log p(x)
                    -\sum_{x\in\mathcal{X}}\sum_{y\in\mathcal{Y}}p(x, y)\log p(y|x) \\
                 &= H(X) + H(Y|X)
        \end{align*}
      \end{tcolorbox}
    \end{frame}

    \subsection{Subsection 2.3: Relative Entropy and Mutual Information}
    \begin{frame}{Conditional Entropy}
      \textit{Relative entropy} or \textit{Kullback-Leibler distance} is a
      measure of distance from $p$ to $q$
      \begin{tcolorbox}[title=Definition]
        Relative entropy between $p(x)$ and $q(x)$ is
        \begin{align*}
          D(p||q) &= \sum_{x\in\mathcal{X}} p(x) \log \frac{p(x)}{q(x)} \\
                  &= E_p \log \frac{p(x)}{q(x)} \\
        \end{align*}
      \end{tcolorbox}
      \textbf{Note: } This is not a true distance and not symmetric
    \end{frame}

    \begin{frame}{Mutual Information}

    \end{frame}

    \begin{frame}[focus]
        Thank you for listening!
    \end{frame}

    \appendix

    \begin{frame}{Question}
        \usebeamercolor[fg]{normal text}
    \end{frame}
\end{document}